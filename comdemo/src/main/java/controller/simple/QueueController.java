package controller.simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: QueueController
 * @author: 张磊
 * @create: 2019/5/27-15:27
 **/

//这是一个简单的消息发送与消息接收
@RestController
public class QueueController {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @RequestMapping("/send")
    public void send(String text){
        //这里的destinationName的值可以为任意值，routKey只是随意取的，不具备实际意义
        jmsMessagingTemplate.convertAndSend("routKey",text);
    }

}
