//package controller.allKinds;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.core.JmsTemplate;
//
//import javax.jms.*;
//
///**
// * @program: Receive
// * @author: 张磊
// * @create: 2019/5/27-16:24
// **/
//
//public class Receive {
//
//
//    @Autowired
//    private JmsTemplate jmsTemplate;
//    /**
//     * 根据消息类型进行对应的处理
//     * @param destination 消息发送/接收共同的Destination
//     * @throws JMSException
//     */
//    public void receive(Destination destination) throws JMSException {
//        Message message = jmsTemplate.receive(destination);
//
//        // 如果是文本消息
//        if (message instanceof TextMessage) {
//            TextMessage tm = (TextMessage) message;
//            System.out.println("from" + destination.toString() + " get textMessage：\t" + tm.getText());
//        }
//
//        // 如果是Map消息
//        if (message instanceof MapMessage) {
//            MapMessage mm = (MapMessage) message;
//            System.out.println("from" + destination.toString() + " get textMessage：\t" + mm.getString("msgId"));
//        }
//
//        // 如果是Object消息
//        if (message instanceof ObjectMessage) {
//            ObjectMessage om = (ObjectMessage) message;
//            ExampleUser exampleUser = (ExampleUser) om.getObject();
//            System.out.println("from" + destination.toString() + " get ObjectMessage：\t"
//                    + ToStringBuilder.reflectionToString(exampleUser));
//        }
//
//        // 如果是bytes消息
//        if (message instanceof BytesMessage) {
//            byte[] b = new byte[1024];
//            int len = -1;
//            BytesMessage bm = (BytesMessage) message;
//            while ((len = bm.readBytes(b)) != -1) {
//                System.out.println(new String(b, 0, len));
//            }
//        }
//
//        // 如果是Stream消息
//        if (message instanceof StreamMessage) {
//            StreamMessage sm = (StreamMessage) message;
//            System.out.println(sm.readString());
//            System.out.println(sm.readInt());
//        }
//    }
//}
