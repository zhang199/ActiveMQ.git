package controller.allKinds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.*;
import java.io.Serializable;

/**
 * @program: SendController
 * @author: 张磊
 * @create: 2019/5/27-15:44
 **/
@RestController
@RequestMapping("/Api")
public class SendController {

    @Autowired
    private JmsTemplate jmsTemplate;

    //Text类型
    @PostMapping("/sendTextMessage")
    public void sendTextMessage(Destination destination,final String message){
        if (null==destination){
            destination=jmsTemplate.getDefaultDestination();
        }
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage(message);
            }
        });
        System.out.println("springJMS send text message: " + message);

    }

    //Map类型
    @PostMapping("/sendMapMessage")
    public void sendMapMessage(Destination destination,final String message){
        if (null==destination){
            destination=jmsTemplate.getDefaultDestination();
        }
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage mapMessage=session.createMapMessage();
                mapMessage.setString("msgId",message);
                return mapMessage;
            }
        });
    }

    //Object类型
    @PostMapping("/sendObjectMessage")
    public void sendObjectMessage(Destination destination, final Serializable object){
        if (null==destination){
            destination=jmsTemplate.getDefaultDestination();
        }
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(object);
            }
        });
        System.out.println("springJMS send object message...");
    }

    //Bytes  字节消息
    @PostMapping("/sendByteMessage")
    public void sendByteMessage(Destination destination,final byte[] bytes){
        if (null==destination){
            destination=jmsTemplate.getDefaultDestination();
        }
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                BytesMessage bytesMessage=session.createBytesMessage();
                bytesMessage.writeBytes(bytes);
                return bytesMessage;
            }
        });
    }


    /**
     * 向默认队列发送Stream消息
     */
    @PostMapping("/sendStreamMessage")
    public void sendStreamMessage(Destination destination) {
        jmsTemplate.send(new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                StreamMessage message = session.createStreamMessage();
                message.writeString("stream string");
                message.writeInt(11111);
                return message;
            }
        });
        System.out.println("springJMS send Strem message...");
    }

}
